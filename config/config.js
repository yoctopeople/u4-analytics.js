var cfg = {
	u4env: process.argv[2] || process.env.U4ENV || "",
    port: process.env.ATTENDEE_API_PORT || 11001,
    mongo: (process.env.MONGODB_URL || "mongodb://localhost:27017/") + (process.argv[2] || process.env.U4ENV || "") + "u4web",
};

module.exports = cfg;