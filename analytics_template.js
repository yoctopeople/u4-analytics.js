[[preload]]

var U4Analytics = U4Analytics || [[config]]

U4Analytics.client = (function () {
	var prepareRequestBody = function (key, value) {
		if (!value) {
			value = {};
		}

		value.clientId = U4Analytics.config.clientId;
		value.session = getSession();

		return JSON.stringify({
			"key": key,
			"value": value
		});
	};

  var createSession = function (){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random()*16)%16 | 0;
      d = Math.floor(d/16);
      return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });

    sessionStorage.setItem("u4session", uuid);
    return uuid;
  };

  var getSession = function (){
    return sessionStorage.getItem("u4session") || createSession();
  };

	var trackAsset = function (u4Handle, attributes) {
		[[trackAssetSection]]
	};

	var pageView = function (u4Handle, attributes) {
		[[pageViewSection]]
	};

  var updateSession = function (u4Handle, attributes) {
    [[updateSessionSection]]
  };



	return {
		pageView: pageView,
		trackAsset: trackAsset,
    updateSession: updateSession
	};
})();

U4Analytics.tealium = (function () {
	var euforeIFrameId = "euforeTealiumFrame";

	var loadScript = function (iframe, src) {
		return new Promise(function (resolve, reject) {
			var script = document.createElement('script');
			script.setAttribute('src', src);
			script.onload = function () {
				console.log(src + "script loaded");
				resolve();
			};

			script.onerror = function () {
				console.log("jquery script failed");
				reject();
			};
			iframe.contentWindow.document.getElementsByTagName('head')[0].appendChild(script);
		})
	};

	var appendTealiumScript = function (iframe, script) {
		iframe.contentWindow.utag_cfg_ovrd = {noview: true};
		return Promise.all([
			loadScript(iframe, "/jquery-3.2.1.min.js"),
			loadScript(iframe, script)
		]);


	};

	var createIFrame = function (u4handle, script) {
		return new Promise(function(resolve, reject) {
			var existedIFrame = document.getElementById(euforeIFrameId + u4handle);

			if (!existedIFrame) {
				existedIFrame = document.createElement('iframe');
				existedIFrame.setAttribute("id", euforeIFrameId + u4handle);
				existedIFrame.style.display = "none";

				existedIFrame.onload = function () {
					appendTealiumScript(existedIFrame, script)
						.then(() => {
							if (!existedIFrame.contentWindow || !existedIFrame.contentWindow.utag) {
								reject(new Error("script not exist"));
							} else {
								resolve(existedIFrame);
							}
						})
						.catch((err) => {
							reject(err);
						});
				};

				document.body.appendChild(existedIFrame);

			} else {
				if (!existedIFrame.contentWindow || !existedIFrame.contentWindow.utag) {
					reject(new Error("script not exist"));
				} else {
					resolve(existedIFrame);
				}
			}


		});
	};

	var simulatePageRefresh = function (u4handle, script, attributes) {
		return createIFrame(u4handle, script).then((iframe)=> {
			return new Promise(function (resolve, reject) {
					iframe.contentWindow.utag.view(attributes,
						function (err) {
							if (!err) {
								resolve();
							} else {
								reject(err);
							}
						}
					);
				});
			});
	};





  var trackMetrics = function (u4handle, script, attributes) {
		return createIFrame(u4handle, script).then((iframe)=> {
			return new Promise(function (resolve, reject) {
				iframe.contentWindow.utag.link(attributes,
					function (err) {
						if (!err) {
							resolve();
						} else {
							reject(err);
						}
					}
				);
			});
		});
	};

	return {
		simulatePageRefresh: simulatePageRefresh,
		pageView: simulatePageRefresh,
		trackMetrics: trackMetrics
	};
})();
