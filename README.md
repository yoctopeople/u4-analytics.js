# Configuration

You could find the configuration variables at the beginning of the analytics.js file.
Change the following variables:

* apiUri: eufore api server uri
* clientId: unique client id
* tealium
    - script: tealium script location
    - metatags: object with meta tags
    - variables: array of custom tealium variables
    - debug: enable teailum debug mode
    - autoTrack: track page on load


# Cart checkout

Method to call:
    U4Analytics.client.cartCheckout(u4Handle, badgeId, assets);

Where:

* u4Handle: eufore configuration name
* badgeId: customer's badge data
* assets: collection of cart's items. Each asset contains title and link.

Example:
```
#!javascript

let u4Handle = "HANDLE1";
let badgeId = "client_badge_1";
let assets = [
   {
      "title": "Example asset 1",
      "link": "http://example.link/asset1"
   },
   {
      "title": "Example asset 2",
      "link": "http://example.link/asset2"
   },
   {
       "title": "Example asset 3",
       "link":" "http://example.link/asset3"
   }
];

U4Analytics.client.cartCheckout(u4Handle, badgeId, assets);
```

# Form submit

Method to call:
    U4Analytics.client.formSubmit(u4Handle, form);

Where:

* u4Handle: eufore configuration name
* form: key-value array


Example:

```
#!javascript

let u4Handle = "HANDLE1";
let form = [
   {
      key: "Name",
      value: "Alex"
   },
   {
      key: "Company",
      value: "Eufore"
   }
];

U4Analytics.client.formSubmit(u4Handle, form);
```


# Track asset

Method to call:
    U4Analytics.client.trackAsset(category, asset);

Where:

* category: asset's category
* asset: asset object with title and link


Example:

```
#!javascript

let category = "assets collection";
let asset = {
   "title": "Example asset 3",
   "link":" "http://example.link/asset3"
};

U4Analytics.client.trackAsset(category, asset);
```


# Tealium

Configuration
In the config section modify the tealium.variables property and add your custom tealium variables.
Then you will be able to use it in the window namespace.
```
#!javascript

window.custom_var = {
    customKey: "custom value"
};
```

Simulate page refresh.
```
#!javascript

U4Analytics.tealium.simulatePageRefresh();
```

Track page view.
```
#!javascript

U4Analytics.tealium.pageView();
```


Track custom metrics
```
#!javascript

U4Analytics.tealium.trackMetrics("type:page", {customKey: "customValue"});
```