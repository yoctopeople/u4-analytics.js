if(u4Handle == [[u4handle]]) {
	var uri = [[u4api]];
	var xhr = new XMLHttpRequest();

	if(attributes && attributes.type == "trackAsset") {
		xhr.open("POST", uri + "/analytics/analytics", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(prepareRequestBody(
			"webAsset",
			{
				category: [[category]],
				asset: [[asset]]
			}));
	} else if (attributes && attributes.type == "formSubmit") {
		xhr.open("POST", uri + "/analytics/form", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(prepareRequestBody(
			"formSubmit",
			{
				u4Handle: [[u4handle]],
				cookie: document.cookie,
				form: [[form]]
			}));
	} else if (attributes && attributes.type == "flag") {
		xhr.open("POST", uri + "/analytics/flag", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(prepareRequestBody(
			"flag",
			{
        u4Handle: [[u4handle]],
				quiz: [[quiz]]
			}));
	}
}