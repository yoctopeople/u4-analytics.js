var digitalData = window.digitalData || { page: {pageInfo:{breadCrumbs:["content", "hpe", "us", "en"]},category:{}}, product:[], user:[]};
digitalData.page.pageInfo.breadCrumbs = digitalData.page.pageInfo.breadCrumbs.concat(["[[id]]", "[[appname]]"]);
digitalData.page.pageID = digitalData.page.pageInfo.breadCrumbs[digitalData.page.pageInfo.breadCrumbs.length-1] + ":" + location.pathname;
digitalData.page.pageInfo.technology_stack = "tealium-selfservice";
(function(){
  var tf = "https://www.hpe.com/global/metrics/bootstrap/prod.js";
  document.write('\x3Cscript type="text/javascript" src="' + tf + '">\x3C/script>');
  var jq = "/jquery-3.2.1.min.js";
  document.write('\x3Cscript type="text/javascript" src="' + jq + '">\x3C/script>');
})();
