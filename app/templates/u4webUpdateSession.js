if (u4Handle == [[u4handle]]) {
  var uri = [[u4api]];
  var xhr = new XMLHttpRequest();

  if(![[name]]) {
    sessionStorage.removeItem("u4session");
  }
  xhr.open("POST", uri + "/analytics/auth", true);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.send(prepareRequestBody(
    "session",
    {
      user: {
        name: [[name]],
        email: [[email]],
        company: [[company]],
        country: [[country]]
      }
    }));

}