/**
 * Created by ashemyakin on 3/14/2017.
 */
const fs = require('fs');
const configRepository = require("../repository/configRepository");

function ConfigService () {

};

ConfigService.prototype.createDemoApp = function () {
	return configRepository.addDemoApp();
};

ConfigService.prototype.createDemoWorkloadApp = function () {
	return configRepository.addDemoWorkloadApp();
};

module.exports = ConfigService;