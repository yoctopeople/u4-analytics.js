/**
 * Created by ashemyakin on 3/14/2017.
 */
const fs = require('fs');
const configRepository = require("../repository/configRepository");

function ScriptService () {
	this.hpeAnalytics = fs.readFileSync("./app/templates/hpeAnalytics.js").toString();
	this.teailumPageView = fs.readFileSync("./app/templates/tealiumPageView.js").toString();
	this.teailumTrackAsset = fs.readFileSync("./app/templates/tealiumTrackAsset.js").toString();
	this.u4webTrackAsset = fs.readFileSync("./app/templates/u4webTrackAsset.js").toString();
	this.u4webPageView = fs.readFileSync("./app/templates/u4webPageView.js").toString();
	this.u4webUpdateSession = fs.readFileSync("./app/templates/u4webUpdateSession.js").toString();
	this.template = fs.readFileSync("./analytics_template.js").toString();
};

ScriptService.prototype.applyMapping = function (mapping) {
	let result = mapping.replace(/"\[\[start\]\]/g, "attributes.");
	result = result.replace(/\[\[end\]\]"/g, " || \"\"");
	return result;
};

ScriptService.prototype.buildPageViewSection = function (u4handle) {
	if (u4handle.type == "tealium") {
        let result = this.teailumPageView.replace(/\[\[u4handle\]\]/g, JSON.stringify(u4handle.u4handle));
        result = result.replace(/\[\[script\]\]/g, JSON.stringify(u4handle.config.script));
        result = result.replace(/\[\[asset\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.pageViewMapping)));
        return result;
	}

    if (u4handle.type == "u4web") {
        let result = this.u4webPageView.replace(/\[\[u4handle\]\]/g, JSON.stringify(u4handle.u4handle));
        return result;
    }

	return "";
};

ScriptService.prototype.buildTrackAssetSection = function (u4handle) {
	if (u4handle.type == "tealium") {
		let result = this.teailumTrackAsset.replace(/\[\[u4handle\]\]/g, JSON.stringify(u4handle.u4handle));
		result = result.replace(/\[\[script\]\]/g, JSON.stringify(u4handle.config.script));
		result = result.replace(/\[\[asset\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping)));
		return result;
	}

    if (u4handle.type == "u4web") {
        let result = this.u4webTrackAsset.replace(/\[\[u4handle\]\]/g, JSON.stringify(u4handle.u4handle));
        result = result.replace(/\[\[u4api\]\]/g, JSON.stringify(u4handle.config.u4api));
        result = result.replace(/\[\[asset\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.asset)));
        result = result.replace(/\[\[category\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.category)));
        result = result.replace(/\[\[form\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.form)));
        result = result.replace(/\[\[quiz\]\]/g, this.applyMapping(JSON.stringify("[[start]]quiz[[end]]")));
        return result;
    }

	return ""
};

ScriptService.prototype.buildUpdateSessionSection = function (u4handle) {
  if (u4handle.type == "u4web") {
    let result = this.u4webUpdateSession.replace(/\[\[u4handle\]\]/g, JSON.stringify(u4handle.u4handle));
    result = result.replace(/\[\[u4api\]\]/g, JSON.stringify(u4handle.config.u4api));
    result = result.replace(/\[\[name\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.name)));
    result = result.replace(/\[\[email\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.email)));
    result = result.replace(/\[\[company\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.company)));
    result = result.replace(/\[\[country\]\]/g, this.applyMapping(JSON.stringify(u4handle.config.assetMapping.country)));
    return result;
  }

  return ""
};

ScriptService.prototype.buildPreloadSection = function (u4handle) {
  if (u4handle.type == "tealium" && (u4handle.u4handle === "U4001TEA" || u4handle.u4handle == "U4TEA70KFTIOT1")) {
    let result = this.hpeAnalytics.replace(/\[\[id\]\]/g, "<R11646>");
    result = result.replace(/\[\[appname\]\]/g, "<IOT APP>");
    return result;
  }

  if (u4handle.type == "tealium" && u4handle.u4handle == "U4TEAHPEWORKLOAD1") {
    let result = this.hpeAnalytics.replace(/\[\[id\]\]/g, "<R11646>");
    result = result.replace(/\[\[appname\]\]/g, "<Right Mix Workload Tool>");
    return result;
  }

  return ""
};

ScriptService.prototype.createScript = function (clientId, applicationId) {
	return configRepository.find(clientId, applicationId)
		.then(cfg => {
			var formatedConfig = {
				config: {
					apiUri: cfg.u4api,
					clientId: cfg.clientId,
					tealium: cfg.teailum
				}
			};

			let result = this.template.replace(/\[\[config\]\]/g, JSON.stringify(formatedConfig));

			let pageViewSection = "";
			let trackAssetSection = "";
			let updateSessionSection = "";
			let preloadSection = "";
			cfg.u4handle.forEach(u4h=> {
        preloadSection += this.buildPreloadSection(u4h);
				pageViewSection += this.buildPageViewSection(u4h);
				trackAssetSection += this.buildTrackAssetSection(u4h);
        updateSessionSection += this.buildUpdateSessionSection(u4h);
			});

			result = result.replace(/\[\[preload\]\]/g, preloadSection);
			result = result.replace(/\[\[pageViewSection\]\]/g, pageViewSection);
			result = result.replace(/\[\[trackAssetSection\]\]/g, trackAssetSection);
			result = result.replace(/\[\[updateSessionSection\]\]/g, updateSessionSection);

			return result;
		});
};

module.exports = ScriptService;