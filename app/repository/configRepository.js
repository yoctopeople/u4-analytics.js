var Promise = require("bluebird");
var ApplicationSchema = require("../schema/applicationSchema");
var ConfigSchema = require("../schema/configSchema");


var configRepository = function() {

};

configRepository.find = function(company, application) {
	return new Promise(function(resolve, reject) {
		var appquery = {
			company: company,
			application: application
		};

		ApplicationSchema
			.findOne(appquery)
			.exec(function(err, doc){
				if (err)
					reject(err);
				else if(!doc) {
					reject(new Error("Application not found"));
				} else {
					var configquery = {
						applicationId: doc._id,
					};

					ConfigSchema.find(configquery)
						.exec(function (confErr, confDoc) {
							if(confErr) {
								reject(confErr);
							} else {
								resolve({
									clientId: doc.clientId,
									u4handle: confDoc
								})
							}
						});
				}
			});
	})
};

configRepository.addConfig = function (u4handle, type, applicationId, config) {
	return new Promise(function (resolve, reject) {
		let configQuery = {
				u4handle: u4handle,
				applicationId: applicationId
			},
			configUpdate = {
				type: type,
				u4handle: u4handle,
				applicationId: applicationId,
				config: config
			},
			configOptions = {
				upsert: true, new: true, setDefaultsOnInsert: true
			};

		ConfigSchema.findOneAndUpdate(configQuery, configUpdate, configOptions, function (error, result) {
			if (error)
				reject(error);

			resolve(result);
		});
	});
};

configRepository.addDemoApp = function () {
	let company = "70kft";
	let application = "iot";
	var that = this;

	return new Promise(function(resolve, reject) {
		let appQuery = {
				company: company,
				application: application
			},
			appUpdate = {
				clientId: "IotReadinessApp"
			},
			appOptions = {
				upsert: true, new: true, setDefaultsOnInsert: true
		};

		ApplicationSchema.findOneAndUpdate(appQuery, appUpdate, appOptions, function (error, result) {
			if (error) reject(error);

			that.addConfig("U4WEB70KFTIOT1", "u4web", result._id, {
					u4api: "http://hpe-u4web-rest-api.eufore.com",
					assetMapping: {
						"category": "[[start]]category[[end]]",
						"asset": "[[start]]asset[[end]]",
						"form": "[[start]]form[[end]]",
						"name": "[[start]]name[[end]]",
						"email": "[[start]]email[[end]]",
						"company": "[[start]]company[[end]]",
						"country": "[[start]]country[[end]]"
					},
					pageViewMapping: {}
			}).then(
				that.addConfig("U4TEA70KFTIOT1", "tealium", result._id, {
					debug: true,
					script: "//tags.tiqcdn.com/utag/yoctodevtea/main/prod/utag.js",
					assetMapping: {
						type: "[[start]]type[[end]]",
						content: "[[start]]pageName[[end]]",
					},
					pageViewMapping: {
						hpmmd: {
							page: {
								name: "[[start]]pageName[[end]]",
								section: "[[start]]section[[end]]",
								events: "[[start]]events[[end]]",
								previous_page: "[[start]]prevPage[[end]]"
							}
						}
					}
				}
			))
				.then(resolve())
				.catch(err=> reject(err));
		});

	});
};

configRepository.addDemoWorkloadApp = function () {
	let company = "mreach";
	let application = "workload";
	var that = this;

	return new Promise(function(resolve, reject) {
		let appQuery = {
				company: company,
				application: application
			},
			appUpdate = {
				clientId: "Workload App"
			},
			appOptions = {
				upsert: true, new: true, setDefaultsOnInsert: true
			};

		ApplicationSchema.findOneAndUpdate(appQuery, appUpdate, appOptions, function (error, result) {
			if (error) reject(error);

			that.addConfig("U4WEBHPEWORKLOAD1", "u4web", result._id, {
				u4api: "http://hpe-u4web-rest-api.eufore.com",
				assetMapping: {
					"category": "[[start]]category[[end]]",
					"asset": "[[start]]asset[[end]]",
					"form": "[[start]]form[[end]]",
					"name": "[[start]]name[[end]]",
					"email": "[[start]]email[[end]]",
					"company": "[[start]]company[[end]]",
					"country": "[[start]]country[[end]]"
				},
				pageViewMapping: {}
			}).then(
				that.addConfig("U4TEAHPEWORKLOAD1", "tealium", result._id, {
						debug: true,
						script: "//tags.tiqcdn.com/utag/yoctodevtea/main/prod/utag.js",
						assetMapping: {
							type: "[[start]]type[[end]]",
							content: "[[start]]pageName[[end]]",
						},
						pageViewMapping: {
							hpmmd: {
								page: {
									name: "[[start]]pageName[[end]]",
									section: "[[start]]section[[end]]",
									events: "[[start]]events[[end]]",
									previous_page: "[[start]]prevPage[[end]]"
								}
							}
						}
					}
				))
				.then(resolve())
				.catch(err=> reject(err));
		});

	});
};

configRepository.findConfigByApplicationId = function(applicationId) {
	return {
		clientId: "70KFT-IotReadinessApp",
		u4handle: [
		{
			name: "U4001WEB",
			type: "u4web",
			config: {
                u4api: "http://sandbox.eufore.com:80861",
				assetMapping: {
					"category": "[[start]]category[[end]]",
					"asset": "[[start]]asset[[end]]"
				},
				pageViewMapping: {}
			}
		},
		{
			name: "U4001TEA",
			type: "tealium",
			config: {
                debug: true,
                script: "//tags.tiqcdn.com/utag/yoctodevtea/main/prod/utag.js",
				assetMapping: {
					type: "[[start]]type[[end]]",
					content: "[[start]]pageName[[end]]",
				},
				pageViewMapping: {
					hpmmd: {
						page: {
							name: "[[start]]pageName[[end]]",
							section: "[[start]]section[[end]]",
							events: "[[start]]events[[end]]",
							previous_page: "[[start]]prevPage[[end]]"
						}
					}
				}
			}
		}]}
};

module.exports = configRepository;