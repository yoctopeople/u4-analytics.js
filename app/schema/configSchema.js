'use strict'

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ConfigSchema = new Schema({
	u4handle: {type: String, required: false, index: true},
	applicationId: {type: String, required: false, index: true},
	type: {type: String, required: false, index: true},
	config: {type: Schema.Types.Mixed, required: false, index: true}
});

module.exports = mongoose.model("Config", ConfigSchema);