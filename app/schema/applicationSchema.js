'use strict'

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ApplicationSchema = new Schema({
    company: {type: String, required: true, index: true},
    application: {type: String, required: true, index: true},
    clientId: {type: String, required: true, index: true}
});

module.exports = mongoose.model("Application", ApplicationSchema);