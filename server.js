var express = require('express');
var compress = require('compression');
var mongoose = require("mongoose");
var cfg = require("./config/config");
var path = require("path");

var app = express();

app.use(compress());

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', req.get('Origin'));
	res.header("Access-Control-Max-Age", "86400");
	res.header("Access-Control-Allow-Headers", "Origin, Cookie, Set-Cookie, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Credentials", true);
	next();
});

var scriptService = require("./app/services/scriptService");
var ScriptService = new scriptService();

var configService = require("./app/services/configService");
var ConfigService = new configService();

app.get("/:client/:app/analytics.js", function (req, res, next) {
	ScriptService.createScript(req.params.client, req.params.app)
		.then(script=> {
			res.type('.js');
			res.send(script);
		})
		.catch((err)=>{
			res.status(400);
			res.send(err);
		});
});

app.get("/createdemoapp", function (req, res, next) {
	ConfigService.createDemoApp()
		.then(()=>{
			res.status(201);
			res.end();
		})
		.catch((err)=>{
			res.status(400);
			res.send(err);
		})
});

app.get("/createdemoworkloadapp", function (req, res, next) {
	ConfigService.createDemoWorkloadApp()
		.then(()=>{
			res.status(201);
			res.end();
		})
		.catch((err)=>{
			res.status(400);
			res.send(err);
		})
});

app.get("/jquery-3.2.1.min.js", function (req, res) {
  res.sendFile(path.resolve(__dirname, 'jquery-3.2.1.min.js'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.end();
});


var connectionString = cfg.mongo;
mongoose.connect(connectionString, {useMongoClient: true });

var db = mongoose.connection;
db.on("error", function (err) {
	console.log("MongoErr: " + err);
	setTimeout(function() {
			mongoose.connect(connectionString, {useMongoClient: true });
		},
		2000);
});

console.log("starting app on port " + (process.env.PORT || cfg.port));
app.listen(process.env.PORT || cfg.port);